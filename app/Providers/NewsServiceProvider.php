<?php

namespace App\Providers;

use App\Services\NewsService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class NewsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $client = new Client([
            'base_uri' => 'https://news.google.com/rss/search'
        ]);
        $this->app->bind(NewsService::class, function () use ($client){
            return new NewsService($client);
        });
    }
}
