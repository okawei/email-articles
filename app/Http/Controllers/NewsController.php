<?php

namespace App\Http\Controllers;

use App\Services\NewsService;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * @var NewsService
     */
    private $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    public function getNews(Request $request){
        $query = $request->get('q');
        if (strpos($query, '@') !== false){
            $query = explode('@', $query)[1];
        }
        return $this->newsService->getArticles($query);
    }
}
