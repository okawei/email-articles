<?php
namespace App\Services;

use DOMWrap\Document;
use Goose\Article;
use Goose\Configuration;
use Goose\Modules\Cleaners\DocumentCleaner;
use Goose\Modules\Extractors\ContentExtractor;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PhpScience\TextRank\TextRankFacade;
use PhpScience\TextRank\Tool\StopWords\English;
use function GuzzleHttp\Promise\settle;
use function GuzzleHttp\Promise\unwrap;

class NewsService{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getArticles(string $query){
        list($links, $titles, $dates, $urls) = $this->getArticleLinks($query);
        $promises = array_map(function($link){
            return $this->client->getAsync($link);
        }, $links);


        $results = unwrap($promises);
        $results = settle($promises)->wait();
        $results = array_map(function($res){
            $body = $res['value']->getBody()->getContents();
            $body = explode('</head>', $body);
            $body = $body[1];
            return $body;
        }, $results);


        foreach ($results as $index=>$result){


            $api = new TextRankFacade();

            $stopWords = new ArticleStopWords();
            $api->setStopWords($stopWords);
            $config = new Configuration([
                // Language - Selects common word dictionary
                //   Supported languages (ISO 639-1):
                //     ar, cs, da, de, en, es, fi, fr, hu, id, it, ja,
                //     ko, nb, nl, no, pl, pt, ru, sv, vi, zh
                'language' => 'en',
                // Minimum image size (bytes)
                'image_min_bytes' => 4500,
                // Maximum image size (bytes)
                'image_max_bytes' => 10,
                // Minimum image size (pixels)
                'image_min_width' => 120,
                // Maximum image size (pixels)
                'image_min_height' => 120,
                // Fetch best image
                'image_fetch_best' => true,
                // Fetch all images
                'image_fetch_all' => false,
                // Guzzle configuration - All values are passed directly to Guzzle
                //   See: http://guzzle.readthedocs.io/en/stable/request-options.html
                'browser' => [
                    'timeout' => 60,
                    'connect_timeout' => 30
                ]]);

            $client = new \Goose\Client();
            $client->setModules('extractors', ['\Goose\Modules\Extractors\ContentExtractor',]);
            $article = $client->extractContent($links[$index], $result);
            $resultText = $article->getCleanedArticleText();
            $results[$index] = $resultText;
            if ($resultText == null){
                $h1Found = strpos($result, $titles[$index].'</h1>');
                $h2Found = strpos($result, $titles[$index].'</h2>');
                $h3Found = strpos($result, $titles[$index].'</h3>');
                $maxFound = max([$h1Found, $h2Found, $h3Found]);
                if ($maxFound !== false){
                    $result = substr($result, $maxFound + strlen($titles[$index]), strlen($result));
                }
                $resultText = preg_replace('/\s+/', ' ', trim(strip_tags(preg_replace('#<script(.*?)>(.*?)</script>#is', '', $result))));
            }
            $results[$index] = [
                'title' => $titles[$index],
                'text' => implode('\n', array_slice($api->summarizeTextCompound($resultText), 0, 5, true)),
                'link' => $urls[$index],
                'posted_on' => $dates[$index]
            ];
        }
        return $results;
    }

    /**
     * @param string $query
     * @return array
     */
    public function getArticleLinks(string $query): array
    {
        $result = $this->client->get('', [
            'query' => [
                'q' => $query
            ]
        ])->getBody()->getContents();
        $object = simplexml_load_string($result);
        $links = [];
        $titles = [];
        $urls = [];
        $dates = [];
        $x = 0;
        foreach ($object->channel->item as $item) {
            $urls[] = (string)$item->link;
            $dates[] = (string)$item->pubDate;
            $titleString = (string)$item->title;
            $titleString = explode(' - ', $titleString);
            unset($titleString[count($titleString)-1]);
            $titleString = implode(' - ', $titleString);
            $titles[] = $titleString;
            $links[] = (string)$item->link;
            $x++;
            if($x === 5){
                break;
            }
        }
        return [$links, $titles, $dates, $urls];

    }
}
